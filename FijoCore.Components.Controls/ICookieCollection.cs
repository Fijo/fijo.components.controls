using System.Collections.Generic;

namespace FijoCore.Components.Controls {
	public interface ICookieCollection : IDictionary<string, string> {
		
	}
}