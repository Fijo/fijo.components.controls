namespace FijoCore.Components.Controls {
	public class Upload : ControlElement {
		public Upload() : base("input", false) {
			Attributes.Set("type", "file");
		}
	}
}