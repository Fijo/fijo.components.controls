using System;

namespace FijoCore.Components.Controls {
	public interface IWebServer {
		string Host { set; }
		void Init();
		void Start();
		void Stop();
		Func<IRequest> Handle { get; }
	}
}