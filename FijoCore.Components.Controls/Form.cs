using System;

namespace FijoCore.Components.Controls {
	public class Form : ControlElement {
		public string Action { get { return Attributes.Get("action"); } set { Attributes.Set("action", value); } }
		public string Enctype { get { return Attributes.Get("enctype"); } set { Attributes.Set("enctype", value); } }
		public FormActions Method { get { return (FormActions) Enum.Parse(typeof(FormActions), Attributes.Get("method"), true); } set { Attributes.Set("method", Enum.GetName(typeof(FormActions), value)); } }
		public string Target { get { return Attributes.Get("src"); } set { Attributes.Set("src", value); } }
		public Form() : base("form", true) {}
	}
}