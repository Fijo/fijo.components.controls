using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI]
	public interface IAppendable<in T> {
		[PublicAPI]
		void Append(T value);
	}
}