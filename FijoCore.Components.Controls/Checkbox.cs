namespace FijoCore.Components.Controls {
	public class Checkbox : ControlElement {
		public string Value { get { return Attributes.Get("value"); } set { Attributes.Set("value", value); } }
		public bool Checked { get { return !string.IsNullOrEmpty(Attributes.Get("value")); } set { Attributes.Set("value", value ? string.Empty : "checked"); } }
		public Checkbox() : base("input", false) {
			Attributes.Set("type", "checkbox");
		}
	}
}