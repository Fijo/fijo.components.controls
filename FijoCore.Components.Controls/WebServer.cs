using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	public class WebServer
	{
		private readonly HttpListener _listener = new HttpListener();
		private readonly Func<HttpListenerRequest, string> _responderMethod;
 
		public WebServer([NotNull] IList<string> handleUrls, [NotNull] Func<HttpListenerRequest, string> method)
		{
			#region PreCondition
			if (!HttpListener.IsSupported) throw new NotSupportedException("Needs Windows XP SP2, Server 2003 or later.");
			if (handleUrls.None()) throw new ArgumentException("handleUrls");
			#endregion

			foreach (var handleUrl in handleUrls) _listener.Prefixes.Add(handleUrl);
 
			_responderMethod = method;
		}

		public void Start() {
			_listener.Start();
		}
 
		public WebServer(Func<HttpListenerRequest, string> method, params string[] prefixes)
			: this(prefixes, method) { }
 
		public void Run()
		{
			ThreadPool.QueueUserWorkItem((o) =>
			                             {
			                             	Console.WriteLine("Webserver running...");
			                             	try
			                             	{
			                             		while (_listener.IsListening)
			                             		{
			                             			ThreadPool.QueueUserWorkItem((c) =>
			                             			                             {
			                             			                             	var ctx = c as HttpListenerContext;
			                             			                             	try
			                             			                             	{
			                             			                             		string rstr = _responderMethod(ctx.Request);
			                             			                             		byte[] buf = Encoding.UTF8.GetBytes(rstr);
			                             			                             		ctx.Response.ContentLength64 = buf.Length;
			                             			                             		ctx.Response.OutputStream.Write(buf, 0, buf.Length);
			                             			                             	}
			                             			                             	catch { } // suppress any exceptions
			                             			                             	finally
			                             			                             	{
			                             			                             		// always close the stream
			                             			                             		ctx.Response.OutputStream.Close();
			                             			                             	}
			                             			                             }, _listener.GetContext());
			                             		}
			                             	}
			                             	catch { } // suppress any exceptions
			                             });
		}
 
		public void Stop()
		{
			_listener.Stop();
			_listener.Close();
		}
	}
}