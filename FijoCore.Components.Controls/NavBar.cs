using System;
using System.Collections.Generic;
using System.Linq;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;

namespace FijoCore.Components.Controls {
	public class NavBar : Container {
		public ControlElement Brand { get; set; }
		public string BrandName { get { return GetBrandTextNode().Text; } set { GetBrandTextNode().Text = value; } }
		private NavBarBehavior _behavior;
		private NavBarPosition _position;
		public NavBarBehavior Behavior {
			get { return _behavior; }
			set {
				_behavior = value;
				AdjustDesign();
			}
		}
		public NavBarPosition Position {
			get { return _position; }
			set {
				_position = value;
				AdjustDesign();
			}
		}
		public NavBarColor Color {
			get { return this.HasClass("navbar-inverse") ? NavBarColor.Black : NavBarColor.White; }
			set {
				var hasClass = this.HasClass("navbar-inverse");
				if (value == NavBarColor.Black) {
					if (!hasClass) this.AddClass("navbar-inverse");
				}
				else if (hasClass) this.RemoveClass("navbar-inverse");
			}
		}

		public NavBar() {
			Class = "navbar";
			var navbar = new Container{Class = "navbar-inner"};
			Brand = new Link {Class = "brand", Target = "#"};
			Brand.Controls.Add(new TextNode());
			navbar.Controls.Add(Brand);
			Controls.Add(navbar);
		}

		private TextNode GetBrandTextNode() {
			return Brand.Controls.OfType<TextNode>().Single();
		}

		private void AdjustDesign() {
			var classesToRemove = GetEnumValues<NavBarBehavior>().SelectMany(x => {
				var isCurrentBehavior = Behavior == x;
				return GetEnumValues<NavBarPosition>()
					.Where(y => !(isCurrentBehavior && y == Position))
					.Select(y => GetDesignClass(x, y));
			});
			ForEachExtention.ForEach(classesToRemove, x => this.RemoveClass(x));
			
			var classToAdjust = GetDesignClass(Behavior, Position);
			if(!this.HasClass(classToAdjust)) this.AddClass(classToAdjust);
		}

		private string GetDesignClass(NavBarBehavior behavior, NavBarPosition position) {
			return string.Format("navbar-{0}-{1}", GetBehaviorDesignPart(behavior), GetPositionDesignPart(position));
		}

		private static IEnumerable<T> GetEnumValues<T>() {
			return Enum.GetValues(typeof(T)).Cast<T>();
		}

		private string GetBehaviorDesignPart(NavBarBehavior behavior) {
			return behavior.ToString().ToLowerInvariant();
		}

		private string GetPositionDesignPart(NavBarPosition position) {
			return position.ToString().ToLowerInvariant();
		}
	}
}