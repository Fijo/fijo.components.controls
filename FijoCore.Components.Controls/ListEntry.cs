namespace FijoCore.Components.Controls {
	public class ListEntry : ControlElement {
		public ListEntry() : base("li", true) {}
	}
}