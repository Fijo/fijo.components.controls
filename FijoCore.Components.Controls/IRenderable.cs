using System.Text;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI]
	public interface IRenderable {
		[PublicAPI]
		void Render([NotNull] StringBuilder sb);
	}
}