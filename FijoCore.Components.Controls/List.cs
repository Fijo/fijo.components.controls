namespace FijoCore.Components.Controls {
	public class List : ControlElement {
		public List(bool isOrdered = false) : base(isOrdered ? "ol" : "ul", true) {}
	}
}