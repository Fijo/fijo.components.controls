using System.Collections.Generic;

namespace FijoCore.Components.Controls {
	public interface IHeaderCollection : IDictionary<string, string> {
		
	}
}