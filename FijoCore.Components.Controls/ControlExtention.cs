using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	public static class ControlExtention {
		public static ControlElement AddClass([NotNull] this ControlElement me, [NotNull] string className) {
			me.Attributes.Append("class", className);
			return me;
		}
		
		// todo improve performance
		public static ControlElement RemoveClass([NotNull] this ControlElement me, [NotNull] string className) {
			var classes = GetClasses(me);
			me.Class = string.Join(" ", classes.Where(x => x != className));
			return me;
		}

		public static IEnumerable<string> GetClasses([NotNull] this ControlElement me) {
			return me.Class.Split(' ');
		}

		public static bool HasClass([NotNull] this ControlElement me, [NotNull] string className) {
			return GetClasses(me).Contains(className);
		}
	}
}