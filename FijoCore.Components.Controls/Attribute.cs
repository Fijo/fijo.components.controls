using System;
using System.Text;
using Fijo.Infrastructure.Model.Pair;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI, Serializable]
	public class Attribute : Pair<string, string>, IAppendable<string>, IRenderable {
		[PublicAPI]
		public string AttrName { get { return First; } }
		[PublicAPI]
		public string AttrValue { get { return Second; } }

		public Attribute() {}

		public Attribute(string first, string second) : base(first, second) {}

		[PublicAPI]
		public void Append(string value) {
			Second += string.Format(" {0}", value);
		}

		public void Render(StringBuilder sb) {
			sb.Append(AttrName);
			sb.Append(@"=""");
			sb.Append(AttrValue);
			sb.Append('"');
		}
	}
}