namespace FijoCore.Components.Controls {
	public class Image : ControlElement {
		public string Src { get { return Attributes.Get("src"); } set { Attributes.Set("src", value); } }
		public Image() : base("img", true) {}
	}
}