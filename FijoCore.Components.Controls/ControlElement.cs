﻿using System;
using System.Text;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI, Serializable]
	public abstract class ControlElement : Control {
		[PublicAPI] protected readonly string TagName;
		[PublicAPI] protected readonly bool UseTagClosing;
		[PublicAPI] public readonly AttributeCollection Attributes = new AttributeCollection();
		[PublicAPI] public readonly ControlCollection Controls = new ControlCollection();
		[PublicAPI] public string Id { get { return Attributes.Get("id"); } set { Attributes.Set("id", value); } }
		[PublicAPI] public string Name { get { return Attributes.Get("name"); } set { Attributes.Set("name", value); } }
		[PublicAPI] public string Class { get { return Attributes.Get("class"); } set { Attributes.Set("class", value); } }

		[PublicAPI]
		protected ControlElement(string tagName, bool useTagClosing) {
			TagName = tagName;
			UseTagClosing = useTagClosing;
		}

		public override void Render(StringBuilder sb) {
			sb.Append('<');
			sb.Append(TagName);
			Attributes.Render(sb);
			if (!UseTagClosing) sb.Append("/");
			sb.Append(">");
			if (UseTagClosing) AddContentAndTagClosing(sb);
		}

		private void AddContentAndTagClosing(StringBuilder sb) {
			Controls.Render(sb);
			sb.Append("</");
			sb.Append(TagName);
			sb.Append('>');
		}
	}
}