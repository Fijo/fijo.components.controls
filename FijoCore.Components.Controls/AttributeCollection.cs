using System;
using System.Text;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI, Serializable]
	public class AttributeCollection : PropertyCollectionBase<Attribute>, IRenderable {
		public void Render(StringBuilder sb) {
			this.ForEach(x => RenderEntry(sb, x));
		}

		private void RenderEntry([NotNull] StringBuilder sb, [NotNull] Attribute x) {
			sb.Append(' ');
			x.Render(sb);
		}
	}
}