namespace FijoCore.Components.Controls {
	public class Hidden : ControlElement {
		public string Value { get { return Attributes.Get("value"); } set { Attributes.Set("value", value); } }
		public Hidden() : base("input", false) {
			Attributes.Set("type", "hidden");
		}
	}
}