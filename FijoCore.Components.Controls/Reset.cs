namespace FijoCore.Components.Controls {
	public class Reset : ControlElement {
		public string Value { get { return Attributes.Get("value"); } set { Attributes.Set("value", value); } }
		public Reset() : base("input", false) {
			Attributes.Set("type", "reset");
		}
	}
}