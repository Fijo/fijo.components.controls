using System;
using System.Collections;
using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic.KeyValuePair;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI, Serializable]
	public abstract class PropertyCollectionBase<T> : IPropertyCollection<T> where T : IPair<string, string>, new() {
		private readonly IDictionary<string, T> _attributes = new Dictionary<string, T>();
		
		#region Implementation of IPropertyCollection<T>
		public IDictionary<string, T> Attributes { get { return _attributes; } }
		public T this[string name] { get { return _attributes[name]; } set { _attributes[name] = value; } }
		#endregion

		#region Implementation of IEnumerable
		public IEnumerator<T> GetEnumerator() {
			return Attributes.Values.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return GetEnumerator();
		}
		#endregion

		#region Implementation of ICollection<T>
		public void Add(T item) {
			Attributes.Add(item.First, item);
		}

		public void Clear() {
			Attributes.Clear();
		}

		public bool Contains(T item) {
			return Attributes.ContainsValue(item);
		}

		public void CopyTo(T[] array, int arrayIndex) {
			Attributes.Values.CopyTo(array, arrayIndex);
		}

		public bool Remove(T item) {
			return Attributes.Values.Remove(item);
		}

		public int Count { get { return Attributes.Count; } }
		public bool IsReadOnly { get { return Attributes.IsReadOnly; } }
		#endregion
	}
}