namespace FijoCore.Components.Controls {
	public class Container : ControlElement {
		public Container() : base("div", true) {}
	}
}