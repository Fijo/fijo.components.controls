using System;
using System.Collections.ObjectModel;
using System.Text;
using FijoCore.Infrastructure.LightContrib.Extentions.IEnumerable.Generic;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI, Serializable]
	public class ControlCollection : Collection<Control>, IRenderable {
		public void Render(StringBuilder sb) {
			this.ForEach(x => x.Render(sb));
		}
	}
}