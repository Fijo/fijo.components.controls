using System.Collections.Generic;

namespace FijoCore.Components.Controls {
	public class Include : ControlElement {
		protected readonly IncludeType Type;
		protected readonly string TargetAttributeName;
		protected readonly static IDictionary<IncludeType, string> TagNames = new Dictionary<IncludeType, string>
		{
			{IncludeType.Style, "link"},
			{IncludeType.Script, "script"}
		};
		
		protected readonly static IDictionary<IncludeType, bool> UseTagClosings = new Dictionary<IncludeType, bool>
		{
			{IncludeType.Style, false},
			{IncludeType.Script, true}
		};
		protected static readonly IDictionary<IncludeType, string> TargetAttributes = new Dictionary<IncludeType, string>
		{
			{IncludeType.Style, "href"},
			{IncludeType.Script, "src"}
		};

		public string Target { get { return Attributes.Get(TargetAttributeName); } set { Attributes.Set(TargetAttributeName, value); } }
		public Include(IncludeType type) : base(TagNames[type], UseTagClosings[type]) {
			Type = type;
			TargetAttributeName = TargetAttributes[type];
			if(type == IncludeType.Style) Attributes.Set("rel", "stylesheet");
		}
	}
}