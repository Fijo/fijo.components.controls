using System;

namespace FijoCore.Components.Controls {
	public class Headline : ControlElement {
		public Headline(byte number) : base(string.Format("h{0}", number), true) {
			#region PreCondition
			if(number < 1 || number > 6) throw new ArgumentOutOfRangeException("argument �number� must be min 1 and max 6 to be a valid html headline element.");
			#endregion
		}
	}
}