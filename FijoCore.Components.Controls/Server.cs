using System.Net;
using System.Text;
using System.Threading;

namespace FijoCore.Components.Controls {
	public class Server {
		public void Start() {
			var server = new WebServer(SendResponse, "http://127.0.0.1/");
			server.Start();
			server.Run();
			Thread.Sleep(300000);
			server.Stop();

		}

		private string SendResponse(HttpListenerRequest arg) {
			//Debug.WriteLine(arg.Url);

			var doc = new Document();
			var head = new Head();
			head.Controls.Add(new Include(IncludeType.Style) {Target = "http://fi-jo.de/object-explorer/css/bootstrap.css"});
			head.Controls.Add(new Include(IncludeType.Style) {Target = "http://fi-jo.de/object-explorer/css/bootstrap-responsive.css"});
			head.Controls.Add(new Include(IncludeType.Script) {Target = "http://code.jquery.com/jquery-latest.js"});
			head.Controls.Add(new Include(IncludeType.Script) {Target = "http://lesscss.googlecode.com/files/less-1.3.0.min.js"});
			head.Controls.Add(new Include(IncludeType.Script) {Target = "http://fi-jo.de/object-explorer/js/bootstrap.min.js"});
			doc.Controls.Add(head);

			var body = new Body();
			body.Controls.Add(new NavBar {BrandName = "Fijo", Behavior = NavBarBehavior.Static, Color = NavBarColor.Black, Position = NavBarPosition.Top});
			var container = new Container{Class = "hero-unit"};
			var headline = new Headline(1);
			headline.Controls.Add(new TextNode{Text = "Hello World with Fijo Controls ;)"});
			container.Controls.Add(headline);
			var content = new P();
			content.Controls.Add(new TextNode{Text = "This is a simple page rendered with .Net using Fijo Controls. To design it I used a little bit of the bootstrap power."});
			container.Controls.Add(content);
			var actions = new P();
			var button = new Link{Class = "btn btn-primary btn-large"};
			button.Controls.Add(new TextNode {Text = "Learn more"});
			actions.Controls.Add(button);
			container.Controls.Add(actions);
			body.Controls.Add(container);
			doc.Controls.Add(body);

			var sb = new StringBuilder();
			doc.Render(sb);
			return sb.ToString();
		}
	}
}