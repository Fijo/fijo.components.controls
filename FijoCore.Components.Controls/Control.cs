using System;
using System.Text;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI, Serializable]
	public abstract class Control : IRenderable {
		#region Implementation of IRenderable
		public abstract void Render(StringBuilder sb);
		#endregion
	}

	[PublicAPI, Serializable]
	public class TextNode : Control {
		[PublicAPI] public string Text { get; set; }

		#region Overrides of Control
		public override void Render(StringBuilder sb) {
			sb.Append(Text);
		}
		#endregion
	}
}