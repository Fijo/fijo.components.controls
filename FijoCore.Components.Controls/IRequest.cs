using System.Text;

namespace FijoCore.Components.Controls {
	public interface IRequest {
		Encoding ContentEncoding { get; }
		ICookieCollection Cookies { get; }
		IHeaderCollection Headers { get; }
		// ...
	}
}