namespace FijoCore.Components.Controls {
	public class Password : ControlElement {
		public string Value { get { return Attributes.Get("value"); } set { Attributes.Set("value", value); } }
		public Password() : base("input", false) {
			Attributes.Set("type", "password");
		}
	}
}