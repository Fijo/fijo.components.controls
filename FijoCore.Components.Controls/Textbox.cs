namespace FijoCore.Components.Controls {
	public class Textbox : ControlElement {
		public string Value { get { return Attributes.Get("value"); } set { Attributes.Set("value", value); } }
		public Textbox() : base("input", false) {
			Attributes.Set("type", "text");
		}
	}
}