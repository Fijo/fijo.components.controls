namespace FijoCore.Components.Controls {
	public class GenericControl : ControlElement {
		public GenericControl(string tag) : base(tag, true) {}
	}
}