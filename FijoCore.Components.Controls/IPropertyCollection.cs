using System.Collections.Generic;
using Fijo.Infrastructure.Model.Pair.Interface.Base;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	[PublicAPI]
	public interface IPropertyCollection<T> : ICollection<T> where T : IPair<string, string>, new() {
		[NotNull, PublicAPI]
		IDictionary<string, T> Attributes { get; }

		[PublicAPI]
		T this[[NotNull] string name] { get; set; }
	}
}