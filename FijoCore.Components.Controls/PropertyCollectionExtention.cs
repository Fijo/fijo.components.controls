using Fijo.Infrastructure.Model.Pair.Interface.Base;
using JetBrains.Annotations;

namespace FijoCore.Components.Controls {
	public static class PropertyCollectionExtention {
		[PublicAPI]
		public static string Get<T>([NotNull] this IPropertyCollection<T> me, [NotNull] string name) where T : IPair<string, string>, new() {
			return me[name].Second;
		}

		[PublicAPI]
		public static string GetOrDefault<T>([NotNull] this IPropertyCollection<T> me, [NotNull] string name) where T : IPair<string, string>, new() {
			T attr;
			return me.Attributes.TryGetValue(name, out attr)
			       	? attr.Second
			       	: default(string);
		}

		[PublicAPI]
		public static void Set<T>([NotNull] this IPropertyCollection<T> me, [NotNull] string name, string value) where T : IPair<string, string>, new() {
			me[name] = Create<T>(name, value);
		}
		
		[PublicAPI]
		public static void Add<T>([NotNull] this IPropertyCollection<T> me, [NotNull] T item) where T : IPair<string, string>, new() {
			me.Attributes.Add(item.First, item);
		}

		[PublicAPI]
		public static void Add<T>([NotNull] this IPropertyCollection<T> me, [NotNull] string name, string value) where T : IPair<string, string>, new() {
			me.Add(Create<T>(name, value));
		}

		[PublicAPI]
		public static void Append<T>([NotNull] this IPropertyCollection<T> me, [NotNull] string name, string value) where T : IPair<string, string>, IAppendable<string>, new() {
			T prop;
			var attributes = me.Attributes;
			if(!attributes.TryGetValue(name, out prop)) attributes.Add(name, Create<T>(name, value));
			prop.Append(value);
		}

		[PublicAPI]
		public static bool Contains<T>([NotNull] this IPropertyCollection<T> me, [NotNull] string name) where T : IPair<string, string>, new() {
			return me.Attributes.ContainsKey(name);
		}

		[PublicAPI]
		public static bool Remove<T>([NotNull] this IPropertyCollection<T> me, [NotNull] string name) where T : IPair<string, string>, new() {
			return me.Attributes.Remove(name);
		}
		
		[Pure]
		private static T Create<T>([NotNull] string name, string value) where T : IPair<string, string>, new() {
			var pair = new T();
			pair.Create(name, value);
			return pair;
		}
	}
}