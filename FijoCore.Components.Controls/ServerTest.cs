using NUnit.Framework;

namespace FijoCore.Components.Controls {
	[TestFixture]
	public class ServerTest {
		[Test]
		public void RunServer() {
			var srv = new Server();

			srv.Start();
		}
	}
}