namespace FijoCore.Components.Controls {
	public class Button : ControlElement {
		public string Value { get { return Attributes.Get("value"); } set { Attributes.Set("value", value); } }
		public Button() : base("input", false) {
			Attributes.Set("type", "button");
		}
	}
}