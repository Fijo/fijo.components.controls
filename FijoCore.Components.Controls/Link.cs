namespace FijoCore.Components.Controls {
	public class Link : ControlElement {
		public string Target { get { return Attributes.Get("href"); } set { Attributes.Set("href", value); } }
		public Link() : base("a", true) {}
	}
}